//
//  AppDelegate.h
//  Kontakty
//
//  Created by Przemek on 25.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsListTableViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

