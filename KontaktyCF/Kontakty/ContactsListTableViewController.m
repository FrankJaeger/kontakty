//
//  ContactsListTableViewController.m
//  Kontakty
//
//  Created by Przemek on 25.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "ContactsListTableViewController.h"


@interface ContactsListTableViewController ()

@property bool haveABAccess;        // Uprawnienia do korzystania z kontaktów.
@property bool sorted;              // Sortowanie.
@property NSMutableArray *people;   // Bufor na tabelę kontaktów.

@property (retain) IBOutlet UIBarButtonItem *sortButton;    // Referencja do przycisku "Sort".

@end


@implementation ContactsListTableViewController

- (IBAction)sortButton:(id)sender {     // Po kliknięciu "Sort" ...
    
    _sorted = !_sorted;                 // Włącz/Wyłącz sortowanie.
    self.sortButton.title = ( _sorted ) ? NSLocalizedString(@"cancel", NULL) : NSLocalizedString(@"sort", NULL);    // Podmień napis na przysku.
    
    [ self viewDidLoad ];   // Odśwież.
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ self checkABAccess ]; // Sprawdza uprawnienia dostępu do kontaktów oraz pyta w przypadku braku.
    _people = [ self getContacts ];    // Pobiera tablicę kontaktów.
    [ self sortButtonCheck ];          // Sprawdza czy uaktywnić przycisk "Sort".
    
    dispatch_async( dispatch_get_main_queue(), ^{
        [ self.tableView reloadData ];      // Przeładowuje dane tableView w main_queue.
    });
    
    
    [ [NSNotificationCenter defaultCenter] addObserver:self         // Wysyła wiadomość "viewDidLoad" po przywróceniu aplikacji z działania w tle.
                                           selector:@selector(viewDidLoad)
                                           name:UIApplicationWillEnterForegroundNotification
                                           object:nil ];

}

- (void) dealloc {
   
    [ [NSNotificationCenter defaultCenter] removeObserver:self ];      // Usuwa "observera" z NC przed zwolnieniem CLTVC.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1; // Return the number of sections.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ( _people ) return [ _people count ]; // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listPrototypeRow" forIndexPath:indexPath];
    
    if ( _haveABAccess ) {
        Contact *contact = [ _people objectAtIndex:indexPath.row ];     // Pobiera kontakt z tablicy people.
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", contact.name, contact.surname];  // Ustawia tekst w komórce na imię i nazwisko danego kontaktu.
    } else {
        cell.textLabel.text = NSLocalizedString(@"access_denied", NULL);    // Wyświetla komunikat o konieczności przyznania uprawnień.
    }
    
    return cell;
}



/* ------------------------------------------------------------------------ */



- (void) checkABAccess {        // Sprawdza uprawnienia.
    
    switch ( ABAddressBookGetAuthorizationStatus() ) {
        case kABAuthorizationStatusAuthorized:
            _haveABAccess = YES;
            break;
            
        case kABAuthorizationStatusNotDetermined:
            [ self requestABAccess ];
            break;
        
        case kABAuthorizationStatusDenied:
        case kABAuthorizationStatusRestricted:
            _haveABAccess = NO;
            break;
    }
    
}

- (void) requestABAccess {      // Prosi o uprawnienia.
    
        ABAddressBookRequestAccessWithCompletion( _addressBook, ^( bool granted, CFErrorRef error )  {
            _haveABAccess = granted;
            
            [self viewDidLoad];     // Odświeża widok po decyzji użytkownika.
    
            if ( error ) NSLog(@"%@: %@", NSStringFromClass([self class]), CFBridgingRelease( error ));
        });
    
}

- (id) getContacts {
    
    if ( _haveABAccess ) {
        CFErrorRef error;
        _addressBook = ABAddressBookCreateWithOptions( NULL, &error );  // Tworzy obiekt książki adresowej.
    
    
        if ( !error && _addressBook ) {     // Gdy wszystko poszło ok...
            CFMutableArrayRef people = (CFMutableArrayRef)ABAddressBookCopyArrayOfAllPeople( _addressBook );    // Skopiuj dane kontaktów z książki do tabeli.
            NSMutableArray *contacts = [[NSMutableArray alloc] init];
        
            if(_sorted) {      // Sortuj po nazwisku, gdy sortowanie jest włączone.
                CFArraySortValues( people, CFRangeMake(0, CFArrayGetCount(people)), (CFComparatorFunction)ABPersonComparePeopleByName, (void *)kABPersonSortByLastName);
            }
            
            for ( int i=0; i < CFArrayGetCount( people ); i++ ) {       // Dla każdego kontaktu w tablicy...
                ABRecordRef person = CFArrayGetValueAtIndex(people, i); // Pobierz rekord.
                [ contacts addObject:[ Contact initWithName:(__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty)
                                                 andSurname:(__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty) ]];  // Dodaj go do bufora.
            }
        
            CFRelease( people );        // Zwalnia niepotrzebne już obiekty.
            CFRelease( _addressBook );
            return contacts;
        
        } else {
            if (error) NSLog( @"%@: %@", NSStringFromClass([ self class ]), CFBridgingRelease( error ) );
            if ( _addressBook ) CFRelease( _addressBook );
        }
    }
    
    return NULL;
}

- (void) sortButtonCheck {      // Sprawdza czy uaktywnić przycisk "Sort".
    
    if ( [ _people count ] > 0 ) {  // Jeżeli tablica _people nie jest pusta...
        [self.sortButton setEnabled:YES];   // Aktywuj.
    } else {
        [self.sortButton setEnabled:NO];    // Deaktywuj.
    }
    
}


@end
