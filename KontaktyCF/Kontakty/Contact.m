//
//  Contact.m
//  Kontakty
//
//  Created by Przemek on 25.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import "Contact.h"

@implementation Contact

+ (id) initWithName:(NSString *)name andSurname:(NSString *)surname {
    Contact *contact = [[Contact alloc] init];
    
    contact.name = name;
    contact.surname = surname;
    
    return contact;
}

@end
