//
//  ContactsListTableViewController.h
//  Kontakty
//
//  Created by Przemek on 25.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"
@import AddressBook;

@interface ContactsListTableViewController : UITableViewController

@property (readonly) ABAddressBookRef addressBook;  

@end
