//
//  Contact.h
//  Kontakty
//
//  Created by Przemek on 25.03.2015.
//  Copyright (c) 2015 Przemysław Foluszczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property NSString *name;
@property NSString *surname;

+ (id) initWithName:(NSString *)name andSurname:(NSString *)surname;

@end
